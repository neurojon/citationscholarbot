The  <strong> Spike Response Model </strong> 
is a generalization of the leaky integrate-and-fire model
and gives a simple description of action potential generation in neurons.
Just as in the integrate-and-fire model, action potentials are generated
when the voltage passes a threshold from below. In contrast to the leaky integrate-and-fire model, the spike response model includes ''refractoriness''.
A notational difference is that integrate-and-fire models
are formulated using differential equations for the 
voltage, whereas the Spike Response Model is formulated using filters.

==References==

* Carandini M, Horton J.C. and Sincich L.M. (2007), Thalamic filtering of retinal spike trains by postsynaptic summation, Journal of Vision (2007) 7:20, 1–11. [http://dx.doi.org/10.1167/7.14.20 doi:10.1167/7.14.20].

