{{Cite journal
| ref = borenstein_2008_combined
| volume = 30
| issue = 12
| pages = 2109–2125
| last1 = Borenstein
| first1 = Eran
| last2 = Ullman
| first2 = Shimon
| title = Combined top-down/bottom-up segmentation
| journal = Pattern Analysis and Machine Intelligence, IEEE Transactions on
| accessdate = 2013-06-30
| archivedate = 2013-06-30
| date = 2008
| url = http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=4408584
}}
