import unittest
from ..parser.templateparser import TemplateParser
import os
import pdb
import codecs
from ..refserver import CrossrefServer, RefServerSingleStub
import json
import warnings


def getOnlineJSON(parsed, jsonfn):
    refserver = CrossrefServer(checkmatch=None)
    json_data = refserver.lookupParsed(parsed)
    if json_data is not None:
        with open(jsonfn, 'w') as outfile:
            outfile.write(json.dumps(json_data))
        print("json file didn't exist. Created.")
    else:
        raise Exception("json file didn't exist and failed to lookup.")
    return json_data


class ParseLookupTestCase(unittest.TestCase):
    """ Tests the ability to parse and lookup the DOI.
        Replace with CitationFixerTestCase?
    """
    def setUp(self):
        self.parser = TemplateParser()
        self.parser.throwErrors = True

    def tearDown(self):
        self.parser = None

    def testBoechlerEtal2010(self):
        self.__test('boechler2010', '10.1103/physrevlett.104.244302')

    def __test(self, testslug, truedoi):
        file_created = False
        with codecs.open(os.path.join(
                         os.path.dirname(os.path.realpath(__file__)),
                         'data', testslug + '.txt'), 'r', 'utf-8') as txtfile:
            text = txtfile.read()
        parsed = self.parser.parse_citation(text)

        parsedfn = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'data', testslug + '.parsed')

        prev_parsed = None
        try:
            with codecs.open(parsedfn, 'r', 'utf-8') as parsedfile:
                prev_parsed = parsedfile.read()
        except IOError:
            with open(parsedfn, 'w') as outfile:
                outfile.write(parsed.dump())
                file_created = True
                pdb.set_trace()
            prev_parsed = None

        jsonfn = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                              'data', 'search.crossref', testslug+'.jsons')

        # if JSON search has changed, need to retrieve JSON results again
        if prev_parsed != parsed.dump():
            with open(parsedfn, 'w') as outfile:
                outfile.write(parsed.dump())
            warnings.warn("Cached parsed file is different "
                          "than the current parse.")
            pdb.set_trace()
            json_data = getOnlineJSON(parsed, jsonfn)
            file_created = True
        else:
            try:
                with open(jsonfn) as jsonfile:
                    json_data = json.loads(jsonfile.read())
                    refserver = RefServerSingleStub(json_data)
                    json_data = refserver.lookupParsed(parsed)
            except IOError as e:
                print "I/O error({0}): {1}".format(e.errno, e.strerror)
                # if file doesn't exist, create it with real json
                warnings.warn("Failed to read cached file:\n\t%s" %
                              jsonfn)
                pdb.set_trace()
                json_data = getOnlineJSON(parsed, jsonfn)
                file_created = True
        self.assertTrue((not file_created) and json_data[0]['doi'] == truedoi)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(ParseLookupTestCase("testBoechlerEtal2010"))
    return suite


def run_suite():
    runner = unittest.TextTestRunner()
    runner.run(suite())
