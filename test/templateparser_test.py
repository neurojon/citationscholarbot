import unittest
from ..parser.templateparser import TemplateParser
import os
import codecs
import re
import pdb


class TemplateParserTestCase(unittest.TestCase):
    def setUp(self):
        self.parser = TemplateParser()
        self.parser.throwErrors = True

    def tearDown(self):
        self.parser = None

    def testAngelucci2002(self):
        self.__testFile('angelucci2002')

    def testAngelucci2002b(self):
        self.__testFile('angelucci2002b')

    def testBalya2002(self):
        self.__testFile('balya2002')

    def testBannen2008(self):
        self.__testFile('bannen2008')

    def testBeneke1998(self):
        self.__testFile('beneke1998')

    def testBorenstein2008(self):
        self.__testFile('borenstein2008')

    def testBoechlerEtal2010(self):
        self.__testFile('boechler2010')

    def testGilli2002(self):
        self.__testFile('gilli2002')

    def testHopfield1995(self):
        self.__testFile('hopfield1995')

    def testBibitem1(self):
        self.__testFile('bibitem1')

    def testBibitem1b(self):
        self.__testFile('bibitem1b')

    def testBibitem2(self):
        self.__testFile('bibitem2')

    def testBibitem3(self):
        self.__testFile('bibitem3')

    def testBibitem4(self):
        self.__testFile('bibitem4')

    def testBibitem5(self):
        self.__testFile('bibitem5')

    def testBibitemetal(self):
        self.__testFile('bibitemetal')

    def __testFile(self, testslug):
        groundtruth = dict()
        csv_fn = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                              'data', testslug+'.truth')
        with codecs.open(csv_fn, 'r', 'utf-8') as csvfile:
            linenum = 1
            for line in csvfile.readlines():
                mat = re.match('^([^\t]+)\t([^\t]+)$', line)
                if not mat:
                    raise Exception(u'Groundtruth for %s has formatting '
                                    u'error in line %d:\n\t"%s"!' %
                                    (testslug, linenum, line))
                groundtruth[mat.group(1)] = mat.group(2).strip()
                linenum += 1
        with codecs.open(os.path.join(os.path.dirname(
                                      os.path.realpath(__file__)),
                                      'data', testslug+'.txt'),
                         'r', 'utf-8') as txtfile:
            text = txtfile.read()
        self.__parseText(text, groundtruth)

    def __parseText(self, text, checks):
        parsed = self.parser.parse_citation(text)
        for var, val in checks.iteritems():
            self.assertTrue(var in parsed,
                            msg=('Variable %s was not parsed! The following '
                                 'keys were parsed: %s' %
                                 (var, parsed.keys())))
            #pdb.set_trace()
            self.assertTrue(''.join(parsed[var]) == val,
                            msg='%s not parsed correctly.\n\t'
                                'Should be: "%s"\n\tParsed as: "%s"' %
                                (var, val, ''.join(parsed[var])))


def suite():
    suite = unittest.TestSuite()
    suite.addTest(TemplateParserTestCase("testAngelucci2002"))
    suite.addTest(TemplateParserTestCase("testAngelucci2002b"))
    suite.addTest(TemplateParserTestCase("testBalya2002"))
    suite.addTest(TemplateParserTestCase("testBannen2008"))
    suite.addTest(TemplateParserTestCase("testBeneke1998"))
    suite.addTest(TemplateParserTestCase("testBorenstein2008"))
    suite.addTest(TemplateParserTestCase("testBoechlerEtal2010"))
    suite.addTest(TemplateParserTestCase("testGilli2002"))
    suite.addTest(TemplateParserTestCase("testHopfield1995"))
    suite.addTest(TemplateParserTestCase("testBibitem1"))
    suite.addTest(TemplateParserTestCase("testBibitem1b"))
    suite.addTest(TemplateParserTestCase("testBibitem2"))
    suite.addTest(TemplateParserTestCase("testBibitem3"))
    suite.addTest(TemplateParserTestCase("testBibitem4"))
    suite.addTest(TemplateParserTestCase("testBibitem5"))
    suite.addTest(TemplateParserTestCase("testBibitemetal"))
    return suite


def run_suite():
    runner = unittest.TextTestRunner()
    runner.run(suite())
