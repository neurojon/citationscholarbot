import unittest
from ..citationfixer import CitationFixer
from ..wikiserver import WikiServerStub
from ..refserver import RefServerStub, RefServerSingleStub, StubCacheError
import os
# import pdb
import json
import glob
import re


class CitationFixerTestCase(unittest.TestCase):
    """ Highest level of testing, tests the CitationFixer.
    """
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testPoormatch1(self):
        self.__testWiki('poormatch1')

    def testGoodmatch1(self):
        self.__testWiki('goodmatch1')

    def testGoodfreestyle1(self):
        self.__testWiki('goodfreestyle1')

    def testGoodfreestyle2(self):
        self.__testWiki('goodfreestyle2')

    def testFreestyleBook1(self):
        self.__testWiki('freestylebook1')

    def testFreestyleBook2(self):
        self.__testWiki('freestylebook2')

    def testNoperiod(self):
        self.__testWiki('noperiod')

    def testBibitem(self):
        self.__testWiki('bibitem')

    def __testWiki(self, testslug):
        wiki = WikiServerStub()
        with open(os.path.join(
                  os.path.dirname(
                      os.path.realpath(__file__)),
                  'data/CitationFixer/', testslug+'.before-wiki'),
                  'rb') as wikifile:
            wiki.setStubPage(testslug, wikifile.read())
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                               'data/CitationFixer/', testslug+'.after-wiki'),
                  'rb') as wikifile:
            truth = wikifile.read()

        refserver = None
        try:
            # test can have single handwritten json file or multiple cached
            fn = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                              'data/CitationFixer/', testslug+'.json')
            with open(fn) as jsonfile:
                json_data = json.load(jsonfile)
            fields_fn = os.path.join(os.path.dirname(
                os.path.realpath(__file__)),
                'data/CitationFixer/', testslug+'.fields')
            with open(fields_fn) as fieldsfile:
                fields = json.load(fieldsfile)

            refserver = RefServerSingleStub(json_data, fields)
        except IOError:
            json_file_pattern = os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                'data/offline.cache/', testslug+'_hash=*.json')
            json_fns = glob.glob(json_file_pattern)
            json_dict = dict()
            for fn in json_fns:
                m = re.search('_hash=(-?\d+)\.json$', fn)
                key = int(m.group(1))
                json_dict[key] = json.load(open(fn))

            fields_file_pattern = os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                'data/offline.cache/', testslug+'_hash=*.fields')
            fields_fns = glob.glob(fields_file_pattern)
            fields_dict = dict()
            for fn in fields_fns:
                m = re.search('_hash=(-?\d+)\.fields$', fn)
                doi_hash = int(m.group(1))
                fields_dict[doi_hash] = json.load(open(fn))
            refserver = RefServerStub(json_dict, fields_dict)

        try:
            cfixer = CitationFixer(refserver)
            original = wiki.getPage(testslug)
            modified = cfixer.fix(original)
            if modified != truth:
                # pdb.set_trace()
                pass
            self.assertTrue(modified == truth)
        except StubCacheError as e:
            print("Created missing cache file(s) for test %s." % testslug)

            # if json file doesn't exist, create it with real json
            if e.key() is not None and e.value() is not None:
                fn = os.path.join(
                    os.path.dirname(os.path.realpath(__file__)),
                    'data/offline.cache/', testslug+'_hash=%s.json' % e.key())
                with open(fn, 'w') as outfile:
                    json.dump(e.value(), outfile)

            if e.doi() is not None and e.fields() is not None:
                fn = os.path.join(
                    os.path.dirname(os.path.realpath(__file__)),
                    'data/offline.cache/',
                    testslug+'_hash=%d.fields' % hash(e.doi()))
                with open(fn, 'w') as outfile:
                    json.dump(e.fields(), outfile)

            self.assertTrue(False,
                            msg=("Cache file(s) missing for %s, created." %
                                 testslug))


def suite(testcase=None):
    suite = unittest.TestSuite()
    if testcase is None:
        #suite.addTest(CitationFixerTestCase("testPoormatch1"))
        suite.addTest(CitationFixerTestCase("testGoodmatch1"))
        suite.addTest(CitationFixerTestCase("testGoodfreestyle1"))
        suite.addTest(CitationFixerTestCase("testGoodfreestyle2"))
        suite.addTest(CitationFixerTestCase("testFreestyleBook1"))
        suite.addTest(CitationFixerTestCase("testFreestyleBook2"))
        suite.addTest(CitationFixerTestCase("testNoperiod"))
    else:
        suite.addTest(CitationFixerTestCase(testcase))
    return suite


def run_suite(testcase=None):
    runner = unittest.TextTestRunner()
    runner.run(suite(testcase))
