from refserver import CrossrefServer
from parser import Reference  # , FreeStyleRef
import re
import pdb
#import difflib


def makeInitials(forename, period=u'.', sep=u' '):
    initials = []
    for part in forename.split(u' '):
        initials.append(part[0]+period)
    return sep.join(initials)


def writeAsCiteTemplate(fields, original=None):
    if fields[u'type'] == u'article-journal':
        text = u'{{Cite journal'
    elif fields[u'type'] == u'book':
        text = u'{{Cite book'
    else:
        pdb.set_trace()
        return None
    if u'author' in fields:
        i = 0
        for author in fields[u'author']:
            initials = makeInitials(fields[u'author'][i][u'given'])
            text += u'|first%d=%s' % (i+1, initials)
            text += u'|last%d=%s' % (i+1, fields[u'author'][i][u'family'])
            i += 1
    if u'editor' in fields and len(fields[u'editor']) > 0:
        pdb.set_trace()  # to-do: add test that has editor(s)
    if u'issued' in fields and 'date-parts' in fields[u'issued']:
        text += u'|year=%s' % fields[u'issued'][u'date-parts'][0][0]

    if u'title' in fields:
        title = fields[u'title']
        # to-do: trust the original title more than that from CrossRef
        # if original is not None:
        #     possible = [original[i:i+len(title)]
        #                         for i in range(len(original)-len(title))]
        #     best = difflib.get_close_matches(title,possible,n=1)[0]
        #     pdb.set_trace()
        text += u'|title=%s' % title
    if u'container-title' in fields:
        if fields['type'] == u'article-journal':
            text += u'|journal=%s' % fields[u'container-title']
        else:
            pdb.set_trace()
    if u'volume' in fields:
        text += u'|volume=%s' % fields[u'volume']
    if u'issue' in fields:
        text += u'|issue=%s' % fields[u'issue']
    if u'page' in fields:
        text += u'|page=%s' % fields[u'page']
    if u'DOI' in fields:
        text += u'|doi=%s' % fields[u'DOI']
    text += u'}}'
    return text


def addDOItoFreestyle(fields, original):
    if u'DOI' in fields:
        if (not fields[u'DOI'] in original and
                not 'doi:' in original and
                not 'dx.doi.org' in original):
            original += u' [http://dx.doi.org/%s doi:%s].' % (
                fields[u'DOI'],
                fields[u'DOI'])
    return original


class CitationFixer:
    def __init__(self, refserver=CrossrefServer()):
        self._refserver = refserver

    def countChanges(self):
        return self.__num_changed

    def incrementNumChanges(self):
        self.__num_changed += 1

    def fix(self, text):
        self.__num_changed = 0

        def _substitute_wpcite(s, loc, toks):
            try:
                json_data = self._refserver.lookupParsed(toks)
                if json_data is None:
                    return None
                # look at how spaces are used in references and use emulate
                # when adding new parameter
                matches = re.findall(
                    ur'(\s*)[a-zA-Z0-9_]+(\s*)='
                    ur'(\s*)[^|=}]*[^ |=}\\n\\r]+(\s*)',
                    s)
                sp = (u'', u'', u'')
                if matches:
                    # for now, just looking at last match
                    sp = matches[len(matches)-1]
                # hack, not sure how to add elements "correctly"
                modified = toks.copy()
                modified.insert(-1, u'|'+sp[0]+'doi'+sp[1]+'='+sp[2])
                modified.insert(-1, json_data[0][u'doi'])
                # use the same whitespace as the previous token
                if len(sp[2]) > 0:
                    modified.insert(-1, sp[2])
                self.incrementNumChanges()
                return modified
            except TypeError:
                pass

        def _write_as_bibitem(s, loc, toks):
            try:
                raw = toks['PotentialReference']
                json_data = self._refserver.lookupRaw(raw)
                if json_data is None:  # should have test case for this
                    return None
                modified = toks.copy()
                tmpl = writeAsCiteTemplate(json_data[0], original=raw)
                if tmpl is None:
                    return None
                if len(modified) == 2:
                    modified[1] = tmpl
                modified['PotentialReference'] = tmpl
                self.incrementNumChanges()
                return modified
            except TypeError:
                pass

        def _add_DOI_to_freestyle(s, loc, toks):
            try:
                raw = toks['PotentialReference']
                json_data = self._refserver.lookupRaw(raw)
                if json_data is None:  # should have test case for this
                    return None
                modified = toks.copy()
                newref = addDOItoFreestyle(json_data[0], original=raw)
                if len(modified) == 2:
                    modified[1] = newref
                modified['PotentialReference'] = newref
                self.incrementNumChanges()
                return modified
            except TypeError:
                pass

        def _dynamic_parse_action(s, loc, toks):
            if toks.getName() == 'FreeStyleRef':
                #return _write_as_bibitem(s, loc, toks)
                return _add_DOI_to_freestyle(s, loc, toks)
            elif (toks.getName() == 'WPCiteTemplate' or
                  toks.getName() == 'BibitemTemplate'):
                return _substitute_wpcite(s, loc, toks)
            else:
                print('Unknown token name "%s".' % toks.getName())
                pdb.set_trace()
                return None

        if text is None:
            #print('Failed to retrieve page "%s". '
            #      'Did you spell it correctly?' % title)
            return(None)
        else:
            # Don't want to set parse action on global parsing string
            ReferenceCopy = Reference.copy()
            for expr in ReferenceCopy.exprs:
                expr.setParseAction(_dynamic_parse_action)
            modified = ReferenceCopy.transformString(text)
            #assert len(ReferenceCopy.exprs) == 2
            return(modified)
