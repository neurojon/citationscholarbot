from wikitools import wiki, api
import random
from collections import OrderedDict
import pdb


class WikiServer:
    def getPage(self, title):
        raise NotImplementedError

    def getPages(self, titles):
        raise NotImplementedError


class MediawikiServer(WikiServer):
    def __init__(self, url):
        self.url = url
        self.__wiki = wiki.Wiki(self.url)
        self._page = None

    def login(self, username, password=False,
              remember=False, force=False, verify=True, domain=None):
        self.__wiki.login(username, password, remember, force, verify, domain)

    def getPage(self, title):
        if not self.__wiki:
            self.__wiki = wiki.Wiki(self.url)
        params = OrderedDict([
            ('action', 'query'),
            ('titles', title),
            ('prop', 'info|revisions'),
            ('intoken', 'edit'),
            ('rvprop', 'timestamp|content')])
        req = api.APIRequest(self.__wiki, params)
        pages = req.query(querycontinue=False)['query']['pages']
        if pages.keys()[0] == u'-1':
            print('Failed to retrieve page "%s". Did you spell it correctly?'
                  % title)
            return None
        self._page = pages[pages.keys()[0]]
        return self._page[u'revisions'][0][u'*']

    def getPages(self, titles):
        if not self.__wiki:
            self.__wiki = wiki.Wiki(self.url)
        params = OrderedDict([
            ('action', 'query'),
            ('titles', titles),
            ('prop', 'info|revisions'),
            ('intoken', 'edit'),
            ('rvprop', 'timestamp|content')])
        req = api.APIRequest(self.__wiki, params)
        pages = req.query(querycontinue=False)['query']['pages']
        if pages.keys()[0] == u'-1':
            print('Failed to retrieve page "%s". Is it spelled correctly?'
                  % titles[0])
            return None
        pdb.set_trace()
        return pages
        #self._page = pages[pages.keys()[0]]
        #return self._page[u'revisions'][0][u'*']

    def getRandomPageNames(self, num_pages):
        if not self.__wiki:
            self.__wiki = wiki.Wiki(self.url)
        params = OrderedDict([
            ('action', 'query'),
            ('list', 'random'),
            ('rnnamespace', '0'),
            ('rnlimit', num_pages),
            ('prop', 'info|revisions'),
            ('intoken', 'edit'),
            ('rvprop', 'timestamp|content')])
        req = api.APIRequest(self.__wiki, params)
        pgs = req.query(querycontinue=False)['query']['random']
        titles = [pg[u'title'] for pg in pgs]
        return titles

    def getEditToken(self):
        return self._page[u'edittoken']

    def getTimestamp(self):
        return self._page[u'revisions'][0]['timestamp']

    def savePage(self, title, content, summary, timestamp, token):
        if not self.__wiki or not self.__wiki.isLoggedIn():
            return None
        params = {
            'action': 'edit',
            'title': title,
            'summary': summary,
            'text': content,
            'basetimestamp': timestamp,
            'token': token,
        }
        req = api.APIRequest(self.__wiki, params)
        res = req.query(querycontinue=False)
        return res

    def getTranscludingPages(self, title, filter_ns=None):
        if not self.__wiki:
            self.__wiki = wiki.Wiki(self.url)
        params = {'action': 'query', 'list': 'embeddedin',
                  'eifilterredir': 'nonredirects',
                  'eititle': title}
        req = api.APIRequest(self.__wiki, params)
        pages = req.query(querycontinue=True)['query']['embeddedin']
        titles = []
        for p in pages:
            if filter_ns is None or p['ns'] == filter_ns:
                titles.append(p['title'])
        random.shuffle(titles)
        return titles


class ScholarpediaServer(MediawikiServer):
    def __init__(self):
        MediawikiServer.__init__(self,
                                 "http://www.scholarpedia.org/w/api.php")


class WikiServerStub(WikiServer):
    __pages = dict()

    def setStubPage(self, title, content):
        self.__pages[title] = content

    def getPage(self, title):
        return self.__pages[title]

    def getPages(self):
        return self.__pages

    def getTitles(self):
        return self.__pages.keys()
