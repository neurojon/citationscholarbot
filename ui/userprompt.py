from getch import getch


def prompt(pmsg, options, errmsg=None):
    user_input = None
    while True:
        print(pmsg)
        for key, msg in options.iteritems():
            print(' (%s) %s' % (key, msg))
        print(' > '),
        user_input = getch()
        print(user_input)
        if user_input in options.keys():
            break
        if not errmsg is None:
            print(errmsg)
    return(user_input)
