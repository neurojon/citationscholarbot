class CitationParser:
    def parse_citation(self, text):
        ''' Pass the text that contains a single reference
            When implemented, should return either a ParsedCitation
            if successful or None.
        '''
        raise NotImplementedError
