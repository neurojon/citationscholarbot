from . import CitationParser
from pyparsing import (Regex, Literal,
                       Optional, ZeroOrMore, FollowedBy,
                       ParseException)
import re
import pdb

ws = Optional(Regex(ur'\s+', re.UNICODE))
Lit = lambda(t): Literal(t) + ws
AnyValue = Regex(u'[^|=}]*[^ |=}\\n\\r]+', re.UNICODE)
AnyURL = Regex(u'[^|}]*[^ |=}\\n\\r]+', re.UNICODE)
_Any = lambda(n): AnyValue.setResultsName(n) + ws
_AnyOrNone = lambda(n): Optional(AnyValue).setResultsName(n) + ws
_URL = lambda(n): AnyURL.setResultsName(n) + ws

citekey_re = re.compile(ur'[-\w_]+\b', re.UNICODE)
CiteKey = Regex(citekey_re, re.UNICODE).setResultsName('CiteKey') + ws

CiteParam = (
    Lit('ref') + Lit('=') + CiteKey |
    Lit('author') + Lit('=') + _Any('Authors') |
    Lit('date') + Lit('=') + _Any('Date') |
    Lit('doi') + Lit('=') + _Any('DOI') |
    Lit('issue') + Lit('=') + _Any('Issue') |
    Lit('journal') + Lit('=') + _Any('Journal') |
    Lit('pages') + Lit('=') + _Any('Pages') |
    Lit('pmc') + Lit('=') + _Any('PMC') |
    Lit('pmid') + Lit('=') + _Any('PMID') |
    Lit('series') + Lit('=') + _Any('Series') |
    Lit('title') + Lit('=') + _Any('Title') |
    Lit('url') + Lit('=') + _URL('URL') |
    Lit('volume') + Lit('=') + _Any('Volume') |
    Lit('year') + Lit('=') + _Any('Year') |

    Lit('last1') + Lit('=') + _Any('last1') |
    Lit('first1') + Lit('=') + _Any('first1') |
    Lit('authorlink1') + Lit('=') + _Any('authorlink1') |
    Lit('last2') + Lit('=') + _Any('last2') |
    Lit('first2') + Lit('=') + _Any('first2') |
    Lit('last3') + Lit('=') + _Any('last3') |
    Lit('first3') + Lit('=') + _Any('first3') |
    Lit('last4') + Lit('=') + _Any('last4') |
    Lit('first4') + Lit('=') + _Any('first4') |
    Lit('last5') + Lit('=') + _Any('last5') |
    Lit('first5') + Lit('=') + _Any('first5') |
    Lit('last6') + Lit('=') + _Any('last6') |
    Lit('first6') + Lit('=') + _Any('first6') |
    Lit('authorlink2') + Lit('=') + _Any('authorlink2') |
    Lit('editor-last') + Lit('=') + _Any('editor-last') |
    Lit('editor-first') + Lit('=') + _Any('editor-first') |
    Lit('editor-link') + Lit('=') + _Any('editor-link') |
    Lit('month') + Lit('=') + _Any('month') |
    Lit('trans_title') + Lit('=') + _Any('trans_title') |
    Lit('location') + Lit('=') + _Any('location') |
    Lit('publisher') + Lit('=') + _Any('publisher') |
    Lit('language') + Lit('=') + _Any('language') |
    Lit('format') + Lit('=') + _Any('format') |
    Lit('type') + Lit('=') + _Any('type') |
    Lit('arxiv') + Lit('=') + _Any('arxiv') |
    Lit('id') + Lit('=') + _Any('id') |
    Lit('isbn') + Lit('=') + _Any('isbn') |
    Lit('issn') + Lit('=') + _Any('issn') |
    Lit('oclc') + Lit('=') + _Any('oclc') |
    Lit('bibcode') + Lit('=') + _Any('bibcode') |
    Lit('accessdate') + Lit('=') + _Any('accessdate') |
    Lit('archiveurl') + Lit('=') + _URL('archiveurl') |
    Lit('archivedate') + Lit('=') + _Any('archivedate') |
    Lit('layurl') + Lit('=') + _URL('layurl') |
    Lit('laysource') + Lit('=') + _Any('laysource') |
    Lit('laydate') + Lit('=') + _Any('laydate') |
    Lit('quote') + Lit('=') + _Any('quote') |
    Lit('ref') + Lit('=') + _Any('ref') |
    Lit('separator') + Lit('=') + _Any('separator') |
    Lit('postscript') + Lit('=') + _Any('postscript')
)
CiteParams = CiteParam + ZeroOrMore(Lit('|') + CiteParam)
WPCiteTemplate = (Regex(u'{{[Cc]ite journal') + ws +
                  Lit('|') + CiteParams + Regex('}}')
                  ).setResultsName('CiteTemplate')

BibitemAuthors1 = (
    Lit('|') + _Any('last1') + Lit('|') + _Any('first1')
)
BibitemAuthors2 = (
    Lit('|') + _Any('last1') + Lit('|') + _Any('first1') +
    Lit('|') + _Any('last2') + Lit('|') + _Any('first2')
)
BibitemAuthors3 = (
    Lit('|') + _Any('last1') + Lit('|') + _Any('first1') +
    Lit('|') + _Any('last2') + Lit('|') + _Any('first2') +
    Lit('|') + _Any('last3') + Lit('|') + _Any('first3')
)
BibitemAuthors4 = (
    Lit('|') + _Any('last1') + Lit('|') + _Any('first1') +
    Lit('|') + _Any('last2') + Lit('|') + _Any('first2') +
    Lit('|') + _Any('last3') + Lit('|') + _Any('first3') +
    Lit('|') + _Any('last4') + Lit('|') + _Any('first4')
)
BibitemAuthors5 = (
    Lit('|') + _Any('last1') + Lit('|') + _Any('first1') +
    Lit('|') + _Any('last2') + Lit('|') + _Any('first2') +
    Lit('|') + _Any('last3') + Lit('|') + _Any('first3') +
    Lit('|') + _Any('last4') + Lit('|') + _Any('first4') +
    Lit('|') + _Any('last5') + Lit('|') + _Any('first5')
)
ExcessAnonParams = ZeroOrMore(Lit('|') + AnyValue + ws + ~FollowedBy(Lit('='))
                              ).setResultsName('Excess')

DOI = Literal(u'doi:') + Regex(ur'[^|\] ]+').setResultsName('DOI')+ws
BibitemOptParams = ZeroOrMore(
    (Lit('|') + Lit('label') + Lit('=') + CiteKey) |
    (Lit('|') + Lit('letter') + Lit('=') +
     Regex(ur'[a-zA-Z]').setResultsName('Letter')) |
    (Lit('|') + Lit('preprint') + Lit('=') +
     Regex(ur'\[\S+\s+') + DOI + Lit(']'))
)
BibitemReqAnonParams = (
    Lit('|') + _AnyOrNone('Title') +
    Lit('|') + _Any('Journal') +
    Lit('|') + _Any('Volume') +
    Lit('|') + _Any('Year') +
    Lit('|') + _Any('Pages')
)
Sp_ = Regex(u'[_ ]')  # spaces in template name - can be space or underline
BibType = Literal('article')
BibitemTemplate = (
    (Regex(u'{{[Bb]ibitem') + Sp_ + BibType + Sp_ + Lit('1') +
     BibitemReqAnonParams + BibitemAuthors1 + ExcessAnonParams +
     BibitemOptParams + Regex('}}')) |
    (Regex(u'{{[Bb]ibitem') + Sp_ + BibType + Sp_ + Lit('2') +
     BibitemReqAnonParams + BibitemAuthors2 + ExcessAnonParams +
     BibitemOptParams + Regex('}}')) |
    (Regex(u'{{[Bb]ibitem') + Sp_ + BibType + Sp_ + Lit('3') +
     BibitemReqAnonParams + BibitemAuthors3 + ExcessAnonParams +
     BibitemOptParams + Regex('}}')) |
    (Regex(u'{{[Bb]ibitem') + Sp_ + BibType + Sp_ + Lit('4') +
        BibitemReqAnonParams + BibitemAuthors4 + ExcessAnonParams +
        BibitemOptParams + Regex('}}')) |
    (Regex(u'{{[Bb]ibitem') + Sp_ + BibType + Sp_ + Lit('5') +
        BibitemReqAnonParams + BibitemAuthors5 + ExcessAnonParams +
        BibitemOptParams + Regex('}}')) |
    (Regex(u'{{[Bb]ibitem') + Sp_ + BibType + Sp_ + Lit('etal') +
        BibitemReqAnonParams + BibitemAuthors1 + ExcessAnonParams +
        BibitemOptParams + Regex('}}'))
).setResultsName('BibitemTemplate')
FreeStyleRef = (
    Regex(ur'^\s*\*?\s*', re.MULTILINE | re.UNICODE) +
    Regex(ur'\w[^\n{}]+$', re.MULTILINE | re.UNICODE
          ).setResultsName('PotentialReference')
).setResultsName('FreeStyleRef')

Reference = (
    WPCiteTemplate.leaveWhitespace() |
    BibitemTemplate.leaveWhitespace() |
    FreeStyleRef.leaveWhitespace()
)


class TemplateParser(CitationParser):
    def __init__(self):
        self.throwErrors = False

    def parse_citation(self, text):
        try:
            parsed = Reference.parseString(text)
            return(parsed)
        except ParseException:
            if self.throwErrors:
                raise
            return(None)


def parsed_as_text(parsed):
    text = ''

    if ('PotentialReference' in parsed and
            'Authors' not in parsed and
            'Title' not in parsed):
        return parsed['PotentialReference']

    if 'Authors' in parsed:
        text += parsed['Authors']
    elif 'last1' in parsed:
        i = 1
        while 'last'+str(i) in parsed:
            if i > 1:
                text += ', '
            if 'first'+str(i) in parsed:
                text += parsed['first'+str(i)]
            text += ' ' + parsed['last'+str(i)]
            i += 1
    else:
        text += 'Anonymous'
        pdb.set_trace()
    if 'Year' in parsed:
        text += ' ('+parsed['Year']+')'
    elif 'Date' in parsed:
        text += ' ('+parsed['Date']+')'
    if 'Title' in parsed:
        text += ' \'' + parsed['Title'] + '\'.'
    if 'Journal' in parsed:
        text += ' '+parsed['Journal']
    if 'Volume' in parsed:
        text += ' '+parsed['Volume']

    return text
