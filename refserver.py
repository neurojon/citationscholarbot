import json
from ui.getch import getch
import urllib
import urllib2 as url
import pdb
import difflib
from parser import parsed_as_text
import re


def standardizeDOI(doi):
    m = re.match(ur'http://dx\.doi\.org/(.*)', doi)
    if m:
        doi = m.group(1)
    return doi


def lookupFieldsFromDOI(doi):
    m = re.match(ur'http://dx\.doi\.org/(.*)', doi)
    if m:
        doi_url = doi
    else:
        doi_url = 'http://dx.doi.org/%s' % doi
    request = url.Request(
        doi_url,
        headers={
            "Accept": "application/vnd.citationstyles.csl+json,"
                      " application/rdf+xml"}
    )
    try:
        response = url.urlopen(request)
    except url.HTTPError:
        return {}
    return json.load(response)


def similar(seq1, seq2):
    return difflib.SequenceMatcher(a=seq1.lower(),
                                   b=seq2.lower()).ratio() > 0.9


def refMatches(parsed, json_data, raw, fields):
    assert fields is not None
    json_data = refMatchesHelper(parsed=parsed,
                                 json_data=json_data,
                                 raw=raw,
                                 fields=fields)
    if json_data is None:
        return None
    for j in range(len(json_data)):
        json_data[j]['doi'] = standardizeDOI(json_data[j]['doi'])
    json_data[0] = dict(json_data[0].items() + fields.items())
    return json_data


def refMatchesHelper(parsed, json_data, raw, fields):
    # make sure last name of first author from json_data either matches
    #  the parsed name or is contained in the raw_data
    if (raw is not None and
            u'author' in fields and
            len(fields[u'author']) > 0 and
            u'family' in fields[u'author'][0] and
            not fields[u'author'][0][u'family'] in raw):
        # to-do make the search fuzzy (use partial_ratio?)
        return None
    elif (parsed is not None and
          fields is not None and
          'last1' in parsed and
          u'author' in fields and
          len(fields[u'author']) > 0 and
          u'family' in fields[u'author'][0] and
          not similar(parsed['last1'], fields[u'author'][0][u'family'])):
        return None
    adj_score = json_data[0]['score']
    if (fields is not None and
            u'issued' in fields and
            u'date-parts' in fields[u'issued']):
        if (parsed is not None and
                u'Year' in parsed and
                not (str(fields[u'issued'][u'date-parts'][0][0])
                     in parsed[u'Year'])):
            adj_score -= .5
        elif (raw is not None and
                not str(fields[u'issued'][u'date-parts'][0][0]) in raw):
            adj_score -= .5

    if adj_score >= 3.75:
        # great match, don't even need to print diagonistic info
        return(json_data)
    elif adj_score < 1.0:
        # such a bad match, don't even need to print diagonistic info
        return None
    else:
        print(u'\n-----------------------------------')
        if parsed is not None:
            print(u'Original:\n\t%s\n' % (parsed_as_text(parsed)))
        else:
            assert(raw is not None)
            print(u'Original:\n\t%s\n' % (raw))
        print(u'From CrossRef:\n\t%s\n' % (json_data[0][u'fullCitation']))
    if adj_score >= 3.3:
        print('Score for reference is high (%.2f:%.2f), accepting.' %
              (adj_score, json_data[0]['score']))
        return(json_data)  # very good match
    if adj_score < 1.5:
        print('Score for reference is low (%.2f:%.2f), skipping.' %
              (adj_score, json_data[0]['score']))
        return None
    # Only scores between 1.5 and 3.3 remain
    if (parsed is not None and 'Title' in parsed and
            similar(parsed['Title'], json_data[0][u'title'])):
        print('Titles similar, accepting.')
        return(json_data)
    else:
        print('Score is %.2f:%.2f.' % (adj_score, json_data[0]['score']))
        print('(U)se DOI from CrossRef, (S)kip, or (B)reak/debug? '),
        user_input = getch()
        if user_input in ['b', 'B']:
            pdb.set_trace()
            print('Skipping\n')
            return None
        if not user_input in ['u', 'U']:
            print('Skipping\n')
            return None
        print('Updating DOI\n')
    return(json_data)


class RefServer:
    def lookupParsed(self, parsed):
        raise NotImplementedError

    def lookupRaw(self, text):
        raise NotImplementedError


class CrossrefServer(RefServer):
    refserver = ("http://search.crossref.org/dois?rows=1&q=")

    def __init__(self, checkmatch=refMatches):
        self._checkmatch = checkmatch
        self._last_json_result = None
        self._last_doi = None
        self._last_fields = None
        assert checkmatch is not None

    def lookupParsed(self, parsed):
        req = self.refserver + urllib.quote_plus(
            parsed_as_text(parsed).encode('utf-8'))
        resp = url.urlopen(req).read()
        json_data = json.loads(resp)
        fields = lookupFieldsFromDOI(json_data[0][u'doi'])
        ret = self._checkmatch(parsed=parsed,
                               json_data=json_data,
                               fields=fields,
                               raw=None)

        self._last_json_result = json_data
        self._last_doi = json_data[0][u'doi']
        self._last_fields = fields
        return ret

    def lookupRaw(self, text):
        req = self.refserver + urllib.quote_plus(text.encode('utf-8'))
        resp = url.urlopen(req).read()
        json_data = json.loads(resp)
        fields = lookupFieldsFromDOI(json_data[0][u'doi'])
        ret = self._checkmatch(json_data=json_data,
                               raw=text,
                               fields=fields,
                               parsed=None)

        self._last_json_result = json_data
        self._last_doi = json_data[0][u'doi']
        self._last_fields = fields
        return ret

    def getLastJsonResult(self):
        return self._last_json_result

    def getLastDOI(self):
        """ DOI found during last lookup.
        """
        return self._last_doi

    def getLastFields(self):
        """ Fields retrieved from DOI during last lookup.
        """
        return self._last_fields


class CacheCrossrefServer(RefServer):
    def __init__(self):
        self._refserver = CrossrefServer()
        self._result = None

    def lookupParsed(self, parsed):
        self._result = self._refserver.lookupParsed(parsed)
        return(self._result)

    def lookupRaw(self, text):
        self._result = self._refserver.lookupRaw(text)
        return(self._result)

    def cached(self):
        return(self._result)


class RefServerSingleStub(RefServer):
    def __init__(self, data, fields):
        self._response = data
        self._fields = fields

    def lookupParsed(self, parsed):
        return refMatches(parsed=parsed,
                          json_data=self._response,
                          fields=self._fields,
                          raw=None)

    def lookupRaw(self, text):
        return refMatches(raw=text,
                          json_data=self._response,
                          fields=self._fields,
                          parsed=None)


class StubCacheError(Exception):
    def __init__(self, key=None, value=None, doi=None, fields=None):
        Exception.__init__(self, "Online data for tests not stored off-line.")
        self._key = key
        self._value = value
        self._doi = doi
        self._fields = fields

    def key(self):
        return self._key

    def value(self):
        return self._value

    def doi(self):
        return self._doi

    def fields(self):
        return self._fields


class RefServerStub(RefServer):
    def __init__(self, data_dict, fields):
        self._json = data_dict
        self._fields = fields

    def getCachedJson(self, text, parsed=None, raw=None):
        if hash(text) not in self._json:
            refsrv = CrossrefServer()
            if parsed is not None:
                refsrv.lookupParsed(parsed)
            else:
                refsrv.lookupRaw(raw)

            json_result = refsrv.getLastJsonResult()
            doi = refsrv.getLastDOI()
            fields = refsrv.getLastFields()
            raise StubCacheError(hash(text),
                                 json_result,
                                 doi=doi,
                                 fields=fields)
        json_data = self._json[hash(text)]
        return json_data

    def getCachedFields(self, doi, parsed=None, raw=None):
        doi = standardizeDOI(doi)
        if hash(doi) not in self._fields:
            refsrv = CrossrefServer()
            if parsed is not None:
                refsrv.lookupParsed(parsed)
            else:
                refsrv.lookupRaw(raw)

            #json_result = refsrv.getLastJsonResult()
            doi2 = standardizeDOI(refsrv.getLastDOI())
            assert doi == doi2
            fields = refsrv.getLastFields()
            raise StubCacheError(doi=doi,
                                 fields=fields)
        return self._fields[hash(doi)]

    def lookupParsed(self, parsed):
        text = parsed_as_text(parsed)
        json_data = self.getCachedJson(text, parsed=parsed)
        fields = self.getCachedFields(json_data[0]['doi'],
                                      parsed=parsed)
        return refMatches(parsed=parsed,
                          json_data=json_data,
                          fields=fields,
                          raw=None)

    def lookupRaw(self, text):
        json_data = self.getCachedJson(text, raw=text)
        fields = self.getCachedFields(json_data[0]['doi'],
                                      raw=text)
        return refMatches(json_data=json_data,
                          raw=text,
                          fields=fields,
                          parsed=None)
