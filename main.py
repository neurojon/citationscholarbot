import test.templateparser_test as tptest
import test.citationfixer_test as cftest
#import test.parse_lookup_test as pltest
import unittest
from wikiserver import ScholarpediaServer
from wikitools import APIError
from citationfixer import CitationFixer
import pdb
import re


def run_tests():
    suite = unittest.TestSuite()
    #suite.addTest(tptest.TemplateParserTestCase("testHopfield1995"))
    #runner = unittest.TextTestRunner()
    #runner.run(suite)
    #return

    suite.addTests(tptest.suite())
    suite.addTests(cftest.suite())
    #suite.addTests(pltest.suite())

    runner = unittest.TextTestRunner()
    runner.run(suite)


def process_page(pagename):
    fixer = CitationFixer()
    ss = ScholarpediaServer()
    original = ss.getPage(pagename)
    modified = fixer.fix(original)
    modified_file = open("modified.txt", "w")
    modified_file.write(modified.encode('utf-8'))
    modified_file.close()
    original_file = open("original.txt", "w")
    original_file.write(original.encode('utf-8'))
    original_file.close()


def get_bib_pages():
    ss = ScholarpediaServer()
    titles = ss.getTranscludingPages('Template:Bibitem article 2', filter_ns=0)
    return titles


def process_pages(min_changes=10, username='Citation Scholar Bot',
                  password=None, pagenames=None, num_pages=10):
    ss = ScholarpediaServer()
    ss.login(username, password)
    fixer = CitationFixer()
    # bibpages = ss.getTranscludingPages('Template:Bibitem article 2',
    #                                    filter_ns=0)
    num_processed = 0
    while (num_processed < num_pages and
            (pagenames is None or
             len(pagenames) != 0)):
        if pagenames is None:
            pagename = ss.getRandomPageNames(num_pages=1)[0]
        else:
            pagename = pagenames.pop()
        print('=======================================================\n')
        print(' Working on ... "%s"' % (pagename))
        original = ss.getPage(pagename)
        # find where the References section begins
        refx = re.search("^\s*(=)+\s*References\s*=+\s*$",
                         original, re.MULTILINE)
        if refx is None:
            continue
        ref0 = original[refx.start():]

        ref1 = fixer.fix(ref0)
        modified = original[:refx.start()] + ref1

        num_changes = fixer.countChanges()
        if num_changes == 0:
            print("No changes made to '%s'" % pagename)
            continue
        if num_changes < min_changes:
            print("Only %d changes made to '%s', moving on." % (num_changes,
                                                                pagename))
            continue
        num_processed += 1
        print("%d changes made to '%s'." % (num_changes, pagename))
        modified_file = open("modified.txt", "w")
        modified_file.write(modified.encode('utf-8'))
        modified_file.close()
        original_file = open("original.txt", "w")
        original_file.write(original.encode('utf-8'))
        original_file.close()
        sav = raw_input('\nWrite the changes to "%s"? ' %
                        pagename)
        print('\n')
        if sav.upper().startswith('Y'):
            timestamp = ss.getTimestamp()
            token = ss.getEditToken()
            summary = (u"Added DOI for %d references according to "
                       u" Scholarpedia's agreement with CrossRef." %
                       (num_changes))
            try:
                ss.savePage(pagename, modified,
                            summary=summary,
                            timestamp=timestamp,
                            token=token)
                with open("saved.txt", "a") as file:
                    file.write(pagename.encode('utf-8'))
                    file.write(u'\n')
            except APIError as e:
                pdb.set_trace()
                print('API Error: %s!' % e.message())
                pass


def test_run_bot(username='Citation Scholar Bot', password=None):
    ss = ScholarpediaServer()
    ss.login(username, password)

    fixer = CitationFixer(wikiserver=ss)
    pagename = 'User:Citation_Scholar_Bot/Tests/Cellular_Neural_Network'

    original = ss.getPage(pagename)
    modified = fixer.fix(original)
    num_changes = fixer.countChanges()

    if num_changes == 0:
        print("No changes made to '%s'" % pagename)
    else:
        timestamp = ss.getTimestamp()
        token = ss.getEditToken()
        summary = u"Testing CitationScholarBot"
        ss.savePage(pagename, modified,
                    summary=summary, timestamp=timestamp, token=token)
